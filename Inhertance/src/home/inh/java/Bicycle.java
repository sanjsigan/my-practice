package home.inh.java;

public class Bicycle {
	public int speed;
	public int gear;

	public Bicycle(int speed, int gear) {
		this.speed = speed;
		this.gear = gear;
	}


	@Override
	public String toString() {
		return ("Bicycle speed=" + speed + ", No of gear=" + gear );
	}

}
