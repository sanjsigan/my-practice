package home.inh.java;

public class Bike extends Bicycle {
	public int SeatHeight;

	public Bike(int speed, int gear, int SeatHeight) {
		super(speed, gear);
		this.SeatHeight = SeatHeight;

	}

	public void setHeight(int newvalue) {
		SeatHeight = newvalue;

	}

	@Override
	public String toString() {
		return (super.toString()+"Bike SeatHeight=" + SeatHeight );
	}

}
