package bcas.oop.exe;

public class HardwareStore {
	private String PartNumber;
	private String PartDescription;
	private int QuanityItem;
	private double PricePerItem;
	

	public HardwareStore(String part, String Description, int quanity, double pricePer) {

		PartNumber = part;
		PartDescription = Description;
		QuanityItem = quanity;
		PricePerItem = pricePer;

	}

	public String getPartNumber() {
		return PartNumber;
	}

	public void setPartNumber(String partNumber) {
		PartNumber = partNumber;
	}

	public String getPartDescription() {
		return PartDescription;
	}

	public void setPartDescription(String partDescription) {
		PartDescription = partDescription;
	}

	public int getQuanityItem() {
		return QuanityItem;
	}

	public void setQuanityItem(int quanityItem) {
		QuanityItem = quanityItem;
	}

	public double getPricePerItem() {
		return PricePerItem;
	}

	public void setPricePerItem(double pricePerItem) {
		PricePerItem = pricePerItem;
	}

	public double getInvoice() {

		double TotalAmount = QuanityItem * PricePerItem;
		return TotalAmount;

	}

}
