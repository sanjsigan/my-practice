package bcas.oop.exe;

public class ExpectionDemo4 {
	public static void main(String[] args) {

		try {
			int x[] = new int[6];
			x[10] = 15;
		} catch (IndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}

	}

}
