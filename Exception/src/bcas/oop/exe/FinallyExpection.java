package bcas.oop.exe;

public class FinallyExpection {
	public static void main(String[] args) {

		try {
			String str = "1234";

			System.out.println(str.length());

			int a = Integer.parseInt(str);

		} catch (NumberFormatException e) {

		} catch (ArrayIndexOutOfBoundsException e) {

		} finally {
			System.out.println("Last Statement..");
		}

	}

}
