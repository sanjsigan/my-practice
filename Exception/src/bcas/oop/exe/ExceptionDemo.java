package bcas.oop.exe;

public class ExceptionDemo {
	public static void main(String[] args) {
		int result = 5 / 1;
		System.out.println(result);

		try {
			double x = 4;
			int y = 0;
			System.out.println(x / y);

		} catch (ArithmeticException a) {
			System.err.println(a.getMessage());
		}
		double x = 5, y = 2;
		System.out.println(x / y);
	}

}
