package bcas.java.sorted;

public class Sorted {
	public int FindMin(int numArray[]) {

		int min = numArray[0];
		for (int i = 1; i < numArray.length; i++) {
			if (min > numArray[i]) {
				min = numArray[i];

			}
		}
		return min;

	}
}
