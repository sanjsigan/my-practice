package bcas.app.oop;

import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class SimpleApplet extends JApplet {

	private JLabel lblName;
	private JLabel lblAddress;
	private JLabel lblEmail;
	private JTextField txtName;
	private JTextField txtAddress;
	private JTextField txtEmail;

	public void init() {

		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());

		lblName = new JLabel("Name");
		lblAddress = new JLabel("Address");
		lblEmail = new JLabel("Email");
		txtName = new JTextField(10);
		txtAddress = new JTextField(10);
		txtEmail = new JTextField(10);

		contentPane.add(lblName);
		contentPane.add(txtName);
		contentPane.add(lblAddress);
		contentPane.add(txtAddress);
		contentPane.add(lblEmail);
		contentPane.add(txtEmail);
	}
}
