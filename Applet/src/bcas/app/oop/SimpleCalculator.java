package bcas.app.oop;

import java.awt.Graphics;

import javax.swing.JApplet;
import javax.swing.JOptionPane;

public class SimpleCalculator extends JApplet {

	double sum;
	double product;
	double difference;
	double quotient;

	public void init() {

		String firstNumber;
		String secondNumber;

		double number1;
		double number2;

		firstNumber = JOptionPane.showInputDialog("Enter the first number");
		secondNumber = JOptionPane.showInputDialog("Enter the second number");
		number1 = Double.parseDouble(firstNumber);
		number2 = Double.parseDouble(secondNumber);

		sum = number1 + number2;
		product = number1 * number2;
		difference = number1 - number2;
		quotient = number1 % number2;
	}

	public void paint(Graphics g) {

		super.paint(g);
		g.drawRect(15, 10, 270, 60);
		g.drawString("Sum " + sum, 25, 25);
		g.drawString("Difference " + difference, 25, 45);
		g.drawString("Quotient " + quotient, 25, 55);
	}

}
