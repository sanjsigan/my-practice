package bcas.app.oop;

import java.applet.Applet;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class TempConVerter extends Applet implements ActionListener {
	JTextField txtInput;
	JLabel lblResult;
	JRadioButton rbCelcius, rbKelvin;

	public void init() {
		Container conpane = getContentPane();
		conpane.setLayout(new FlowLayout());
		txtInput = new JTextField("", 10);
		conpane.add(txtInput);
		rbCelcius = new JRadioButton("to Celcius", true);
		conpane.add(rbCelcius);
		rbKelvin = new JRadioButton("to Kelvin", false);
		conpane.add(rbKelvin);
		ButtonGroup selection = new ButtonGroup();
		selection.add(rbCelcius);
		selection.add(rbKelvin);
		JButton button1 = new JButton("Show Result");
		button1.addActionListener(this);
		conpane.add(button1);
		lblResult = new JLabel("Enter Ferenheit, Choose an option to convert and Click Show Result");
		conpane.add(lblResult);
	}

	private Container getContentPane() {
		// TODO Auto-generated method stub
		return null;
	}

	public void actionPerformed(ActionEvent e) {
		DecimalFormat df = new DecimalFormat("#.##");
		double ferenheit = Double.parseDouble(txtInput.getText());
		double answer = 0.0;
		answer = ((5.0 / 9.0) * (ferenheit - 32.0));
		if (rbKelvin.isSelected())
			answer += 273.15;
		lblResult.setText(String.valueOf(df.format(answer)));
	}
}
