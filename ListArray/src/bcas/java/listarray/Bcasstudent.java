package bcas.java.listarray;

public class Bcasstudent {
	private String name;
	private int marks;
    private String batch;
    
	public Bcasstudent(String name, int marks, String batch) {
		
		this.name = name;
		this.marks = marks;
		this.batch = batch;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@Override
	public String toString() {
		return "Bcasstudent [name=" + name + ", marks=" + marks + ", batch=" + batch + "]";
	}
    
	

}
