package bcas.java.listarray;

import java.util.ArrayList;

public class ListOfArray {
	public static void main(String[] args) {
		ArrayList<String> nameList = new ArrayList<>();
		nameList.add("kamal");
		nameList.add("Santhan");
		nameList.add("Vimal");

		for (String name : nameList) {
			System.out.println(name);

		}
		System.out.println();
		System.out.println(nameList.get(1));

	}

}
