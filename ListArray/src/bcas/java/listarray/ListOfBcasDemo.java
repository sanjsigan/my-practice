package bcas.java.listarray;

import java.awt.List;
import java.util.ArrayList;

public class ListOfBcasDemo {
	public static void main(String[] args) {

		ArrayList<Bcasstudent> bcasStudent = new ArrayList<>();

		Bcasstudent CSD12_1 = new Bcasstudent("kamal", 54, "CSD12");
		Bcasstudent CSD13_1 = new Bcasstudent("kamal", 84, "CSD13");

		bcasStudent.add(CSD12_1);
		bcasStudent.add(CSD13_1);

		for (Bcasstudent bcasDetails : bcasStudent) {
			System.out.println(bcasDetails);

		}

	}
}
