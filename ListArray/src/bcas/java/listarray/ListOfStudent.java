package bcas.java.listarray;

import java.util.ArrayList;

public class ListOfStudent {
	public static void main(String[] args) {
		ArrayList<StudentList> student = new ArrayList<>();

		StudentList list1 = new StudentList("Thevapalan", 84);
		StudentList list2 = new StudentList("Nanthakopalan", 78);

		student.add(list1);
		student.add(list2);
                                     
		for (StudentList listOfStudent : student) {
			System.out.println(listOfStudent);

		}
		System.out.println();

		System.out.println(student.get(0).getName() + " = " + student.get(0).getMarks());

	}

}
