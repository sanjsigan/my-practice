package bcas.java.pqueue;

public class MyQueuee {

	private static final int capacity = 5;
	int arr[] = new int[capacity];
	int size = 0;
	int top = -1;
	int rear = 0;

	public void enqueue(int pushedElement) {
		if (top < capacity - 1) {
			top++;
			arr[top] = pushedElement;

		} else {
			System.out.println("over flow!");
		}
	}

	public void dequeue() {
		if (top >= rear) {
			rear++;

			display();
		} else {
			System.out.println("under flow!");
		}
	}

	public boolean isEmpty() {
		return (rear == 0);
	}

	public boolean isFull() {
		return (rear == capacity);
	}

	public void display() {
		if (top >= rear) {

			for (int i = rear; i <= top; i++) {
				System.out.println(arr[i]);
			}
		}
	}
}
