package bcas.java.pqueue;

public class QueueDemo {

	public static void main(String[] args) {
		MyQueuee queueDemo = new MyQueuee();

		queueDemo.enqueue(20);
		queueDemo.enqueue(10);
		queueDemo.enqueue(50);
		queueDemo.enqueue(13);
		queueDemo.enqueue(45);

		queueDemo.display();

	}
}
