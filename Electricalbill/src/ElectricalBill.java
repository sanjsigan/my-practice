import java.util.Scanner;

public class ElectricalBill {
	public static void main(String args[]) {
		boolean fact = false;
		int units = 0;
		double amount = 0.0;
		Scanner scan = new Scanner(System.in);
		do {
			System.out.println("Enter number of unit you consumed: ");
			units = scan.nextInt();

			if (0 < units && units <= 100) {
				amount = (units * 5);
			} else if (units > 100 && units <= 200) {
				amount = ((100 * 5) + (units - 100) * 7);
			} else if (units > 200 && units <= 300) {
				amount = ((100 * 5) + (100 * 7) + (units - 200) * 10);
			} else if (units > 300) {
				amount = ((100 * 5) + (100 * 7) + (100 * 10) + (units - 300) * 15);
			} else {
				amount = 0;
			}
			System.out.println("Total amount is: " + amount);
		} while (!fact);
	}
}