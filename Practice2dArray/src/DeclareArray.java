import java.util.Random;
import java.util.Scanner;

public class DeclareArray implements ArrayMatrix {

	Random ran = new Random();

	public int row;
	public int col;

	@Override
	public int[][] arrayRandom() {
		int[][] arrayMatrix = new int[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				int randValue = ran.nextInt(50);
				if (randValue > 9) {
					arrayMatrix[i][j] = randValue;
				} else {
					j--;
				}

			}
		}
		return arrayMatrix;
	}

	@Override
	public void printArray(int[][] matrix) {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print("[" + matrix[i][j] + "]" + " ");
			}
			System.out.println();
		}

	}

	@Override
	public int[][] sum(int[][] matrixA, int[][] matrixB) {
		int sum[][] = new int[row][col];

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				sum[i][j] = matrixA[i][j] + matrixB[i][j];
			}
		}
		return sum;
	}

}
