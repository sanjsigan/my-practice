
public interface ArrayMatrix {

	public int[][] arrayRandom();

	public void printArray(int matrix[][]);

	public int[][] sum(int[][] matrixA, int[][] matrixB);
}
