import java.util.Scanner;

public class ArrayDemo extends DeclareArray {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		DeclareArray array = new DeclareArray();

		System.out.println("Enter number of  Rows");

		array.col = scan.nextInt();

		System.out.println("Enter number of Columns:");

		array.row = scan.nextInt();
		int[][] matrixA = array.arrayRandom();
		int[][] matrixB = array.arrayRandom();

		array.printArray(matrixA);
		System.out.println("\n**********************\n");
		array.printArray(matrixB);
		System.out.println("\n**********************\n");
		array.printArray(array.sum(matrixA, matrixB));
		
		System.out.println("\n**********************\n");

	}

}
