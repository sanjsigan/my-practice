package home.inter.java;

public class Bicycle implements Vehicles {
	int speed;
	int gear;

	@Override
	public void changeGear(int NewGear) {
		gear = NewGear;

	}

	@Override
	public void SpeedUp(int increment) {
		speed = speed + increment;
	}

	@Override
	public void Breakes(int decrement) {
		speed = speed - decrement;

	}

	public void PrintStates() {
		System.out.println("speed=" + speed + ", gear=" + gear);
	}

}
