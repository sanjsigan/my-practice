package bcas.order.oop;

public abstract class VegPizza extends Pizza {

	
	public abstract String name();

	
	public abstract String size();

	
	public abstract float price();
        
        
}
