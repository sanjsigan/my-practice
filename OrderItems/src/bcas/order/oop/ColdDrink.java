package bcas.order.oop;

public abstract class ColdDrink implements Item {
    public abstract float price();
}
