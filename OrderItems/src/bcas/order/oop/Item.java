package bcas.order.oop;

public interface Item {
	public String name();

	public String size();

	public float price();

}
