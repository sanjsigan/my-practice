
public class BubbleSortExample {
	public static void BubbleSort(int[] arr) {

		int n = arr.length;
		int index = 0;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (arr[i] > arr[j]) {
					index = arr[i];
					arr[i] = arr[j];
					arr[j] = index;
				}

			}
			printArray(arr);

		}

	}

	private static void printArray(int[] arr) {

		System.out.println();

		for (int j = 0; j < arr.length; j++) {
			System.out.print(+arr[j] + " ");
		}

	}

	public static void main(String[] args) {

		int arr[] = { 5, 3, 4, 2, 1 };
		System.out.print("Before sort: ");
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + " ");

		}

		BubbleSort(arr);

		System.out.println();
		System.out.print("Array After Bubble Sort:  ");
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + " ");

		}

	}

}
