package bcas.oop.abs;

public class Contractor extends Employee {
	private int workingHours;

	public Contractor(String name, int paymentPerHour) {
		super(name, paymentPerHour);

	}

	@Override
	public int calculateSalary() {

		return getPaymentPerHour() * 8;
	}

}
