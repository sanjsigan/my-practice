
package bcas.java.many;

public class ManyStack {

	int maxSize;
	long stackArray[];
	int top[];

	public ManyStack(int NumberofStack, int SizeOfStack) {

		
		maxSize = SizeOfStack * NumberofStack;
		stackArray = new long[maxSize];

		top = new int[NumberofStack];
		for (int i = 0; i < NumberofStack; i++) {
			top[i] = ((i * SizeOfStack) - 1);

		}
	}

	public void push(int location, int value) {
		top[location] += 1;
		stackArray[top[location]] = value;

	}

	public void printStack() {
		for (long l : stackArray) {
			System.out.print(l+" ");

		}

	}

}
