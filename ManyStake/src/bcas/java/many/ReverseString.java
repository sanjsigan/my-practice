package bcas.java.many;

public class ReverseString {
	String Reverse = "";
	char[] stringToCharArray = Reverse.toCharArray();
	int top;
	private int mySize;

	public ReverseString(int mySize) {
		this.mySize = mySize;
		stringToCharArray = new char[mySize];
		top = -1;

	}

	public void push(char pushedElement) {
		stringToCharArray[++top] = pushedElement;

	}

	public char pop() {

		return stringToCharArray[top--];

	}

	public char peek() {
		return stringToCharArray[top];

	}

	public boolean isEmpty() {
		return (top == -1);

	}

	public boolean isFull() {
		return (top == mySize - 1);

	}

}
