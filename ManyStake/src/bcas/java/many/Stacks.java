package bcas.java.many;

public interface Stacks {
	public void push1(int pushValue);

	public void push2(int pushValue);

	public void push3(int pushValue);

	public int pop1();

	public int pop2();

	public int pop3();

}
