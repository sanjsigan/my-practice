package bcas.java.joinerr;

import java.util.ArrayList;
import java.util.List;

public class StringJoiner {
	public static void main(String[] args) {
		java.util.StringJoiner joiner = new java.util.StringJoiner(",", "{ ", " }");

		joiner.add("A");
		joiner.add("B");
		joiner.add("C").add("D");

		System.out.println(joiner);

		List<String> name = new ArrayList<String>();
		

		name.add("Sanjsi");
		name.add("Mathav");

		// name.forEach(System.out::print);

		name.forEach(listName -> System.out.print(listName + ", "));

	}

}
