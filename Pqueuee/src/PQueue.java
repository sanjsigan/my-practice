import java.util.concurrent.PriorityBlockingQueue;

public class PQueue {
	public static void main(String[] args) {
		PriorityBlockingQueue<Integer> pQueue = new PriorityBlockingQueue<>();

		pQueue.add(10);
		pQueue.add(8);
		pQueue.add(32);
		pQueue.add(28);
		pQueue.add(18);

		while (!pQueue.isEmpty()) {
			System.out.println(pQueue.poll());

		}

	}

}
