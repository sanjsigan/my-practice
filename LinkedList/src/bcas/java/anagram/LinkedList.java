package bcas.java.anagram;

import javax.xml.soap.Node;

public class LinkedList {
	private Node start;
	private Node end;
	public int size;

	public LinkedList() {
		start = null;
		end = null;
		size = 0;

	}

	public void insertAtStart(String val) {
		Node node = new Node(val, null);
		size++;
		if (start == null) {
			start = node;
			end = start;
		} else {
			node.setLink(start);
			start = node;
		}
	}

	
	public void insertAtEnd(String val) {
		Node node = new Node(val, null);
		size++;
		if (start == null) {
			start = node;
			end = start;
		} else {
			end.setLink(node);
			end = node;
		}
	}


	public void insertAtPos(String val, int pos) {
		Node nptr = new Node(val, null);
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size; i++) {
			if (i == pos) {
				Node tmp = ptr.getLink();
				ptr.setLink(nptr);
				nptr.setLink(tmp);
				break;
			}
			ptr = ptr.getLink();
		}
		size++;
	}

	public void deleteAtPos(int pos) {
		if (pos == 1) {
			start = start.getLink();
			size--;
			return;
		}
		if (pos == size) {
			Node s = start;
			Node t = start;
			while (s != end) {
				t = s;
				s = s.getLink();
			}
			end = t;
			end.setLink(null);
			size--;
			return;
		}
		Node ptr = start;
		pos = pos - 1;
		for (int i = 1; i < size - 1; i++) {
			if (i == pos) {
				Node tmp = ptr.getLink();
				tmp = tmp.getLink();
				ptr.setLink(tmp);
				break;
			}
			ptr = ptr.getLink();
		}
		size--;
	}


	public boolean isEmpty() {
		return start == null;
	}

	
	public int getSize() {
		return size;
	}

	public void display() {
		
		if (size == 0) {
			System.out.println("empty");
			return;

		}
		if (start.getLink() == null) {
			System.out.println(start.getData());
			return;
		}
		Node node = start;
		System.out.print(start.getData() + "->");
		node = start.getLink();
		while (node.getLink() != null) {
			System.out.print(node.getData() + "->");
			node = node.getLink();
		}
		System.out.println(node.getData());

	}

}
