package bcas.java.anagram;

import org.w3c.dom.Node;

public class NodeAnagram {
	private int data;
	private Node Link;

	public NodeAnagram(int d, Node n) {
		data = d;
		Link = n;

	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getLink() {
		return Link;
	}

	public void setLink(Node link) {
		Link = link;
	}

}
