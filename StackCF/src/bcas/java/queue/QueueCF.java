package bcas.java.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueCF {
	public static void main(String[] args) {
		Queue<String> nameQueue = new LinkedList<>();
		nameQueue.add("Siva");
		nameQueue.add("Mathav");

		/*
		 * for (String string : nameQueue) { System.out.println(string);
		 * 
		 * }
		 */
		System.out.println();
		while (!nameQueue.isEmpty()) {
			System.out.println(nameQueue.poll());
		}

	}

}
