package bcas.java.stackcf;

import java.util.Stack;

public class StackCF {
	public static void main(String[] args) {
		Stack<String> nameStack = new Stack<>();

		nameStack.push("Ram");
		nameStack.push("Mathav");
		nameStack.push("Kumar");

		System.out.println(nameStack.pop());

		for (String string : nameStack) {
			System.out.println(string);

		}
		System.out.println();
		while (!nameStack.isEmpty()) {
			System.out.println(nameStack.pop());
		}

	}

}
