package bcas.java.palin;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class PalindromeCheck {
	private static Stack<Character> charStack;
	private static Queue<Character> charQueue;

	public static boolean checkPlaindrome(String name) {
		charStack = new Stack<>();
		charQueue = new LinkedList<>();

		char[] charName = name.toUpperCase().toCharArray();
		for (char c : charName) {
			if (c != ' ') {
				charQueue.add(c);
				charStack.push(c);
			}
		}

		while (!charStack.isEmpty()) {
			if (charStack.pop() != charQueue.poll()) {
				return false;

			}
		}

		return true;

	}

}
