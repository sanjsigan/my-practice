package bcas.java.palin;

import java.util.Stack;

public class Brackets {
	private static Stack<Character> charStack;

	public static boolean MatchBrackets(String bracket) {
		charStack = new Stack<>();

		char[] charBracket = bracket.toCharArray();
		for (Character character : charStack) {
			if (character == '{')
				return false;
			if (character == '(')
				charStack.push(character);

			if (character == '{')
				charStack.push(character);
			if (character == '}' && charStack.isEmpty())
				return false;
			else if (charStack.peek() == '(')
				charStack.pop();
			else
				return false;

		}
		return charStack.isEmpty();
	}

}
