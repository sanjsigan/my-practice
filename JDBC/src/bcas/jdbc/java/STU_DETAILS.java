package bcas.jdbc.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class STU_DETAILS {
	// JDBC driver name and database URLF
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/java";
	// Database credentials
	static final String USER = "root";
	static final String PASS = "sanjsi";

	public static void main(String[] args) {

		Connection conn = null;
		Statement stmt = null;

		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating table in given database...");
			stmt = conn.createStatement();

			String sql = "DROP TABLE STU_MARKS";
			stmt.executeUpdate(sql);

			sql = "UPDATE STU_MARKS SET last = ? , " + "Marks_Maths = ? " + "WHERE id = ?";

			sql = "CREATE TABLE STU_MARKS (id int not NULL,  first VARCHAR(255), last VARCHAR(255),  Marks_Maths int,Marks_tamil int,Marks_Science int, PRIMARY KEY ( id ));";
			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
			System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();
			sql = "INSERT INTO STU_MARKS VALUES (100, 'Kriss', 'Kurian', 85,58,75);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO STU_MARKS VALUES (101, 'Enrique', 'John', 74,89,75);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO STU_MARKS values (102, 'Taylor', 'Swift', 80,68,70);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO  STU_MARKS VALUES(103, 'Linkin', 'Park', 68,54,42);";
			stmt.executeUpdate(sql);
			System.out.println("Inserted records into the table...");

			sql = "SELECT id, first, last, Marks_Maths,Marks_Tamil,Marks_Science FROM STU_MARKS;";
			ResultSet rs = stmt.executeQuery(sql);

			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				String first = rs.getString("first");
				String last = rs.getString("last");
				int Marks_Maths = rs.getInt("Marks_Maths");
				int Marks_Tamil = rs.getInt("Marks_Tamil");
				int Marks_Science = rs.getInt("Marks_Science");
				// Display values
				System.out.print("ID: " + id);
				System.out.println(", First: " + first);
				System.out.print(", Last: " + last);
				System.out.print(", Marks_Maths: " + Marks_Maths);
				System.out.print(", Marks_Tamil: " + Marks_Tamil);
				System.out.println(", Marks_Science: " + Marks_Science);

			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			System.out.println("Goodbye!");
		}
	}

}