package bcas.java.sort3;

public class FindSorted {
	public static void findSort(int[] arr) {
		int n = arr.length, swap = 0;
		for (int i = 1; i < n; i++) {

			for (int j = i; j > 0; j--) {
				if (arr[j] < arr[j - 1]) {
					swap = arr[j];
					arr[j] = arr[j - 1];
					arr[j - 1] = swap;
				}
			}
		}
	}
    
	public static void main(String[] args) {
		int arr[] = { 5, 2, 1, 3, 6, 0, 4 };
		System.out.print("Before Sort: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		findSort(arr);
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
