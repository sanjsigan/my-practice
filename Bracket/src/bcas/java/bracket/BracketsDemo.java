package bcas.java.bracket;

import java.util.Scanner;
import java.util.Stack;

public class BracketsDemo {
	public static void main(String[] args) {
		boolean fact = true;
		do {

			Scanner scan = new Scanner(System.in);

			String str = null;
			Stack<Character> stack = new Stack<>();
			System.out.println("Enter Your Brackets");
			str = scan.next();

			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) == '{' || str.charAt(i) == '[' || str.charAt(i) == '(') {
					stack.push(str.charAt(i));

				} else if (!stack.empty() && ((str.charAt(i) == ']' && stack.peek() == '[')
						|| (str.charAt(i) == '}' && stack.peek() == '{')
						|| (str.charAt(i) == ')' && stack.peek() == '('))) {
					stack.pop();

				} else {
					stack.push(str.charAt(i));
				}
			}
			if (stack.empty()) {
				System.out.println("Correct");
			} else {
				System.out.println("Wrong");
				
			}
		} while (fact);
	}

}
