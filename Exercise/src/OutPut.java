import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class OutPut {

	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/new";

	// Database credentials

	static final String USER = "root";
	static final String PASS = "sanjsi";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to database ....");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.close();
			System.out.println("Connection Sucess..");
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException se) {

			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}

		}
		System.out.println("Goodbye");
	}
}
