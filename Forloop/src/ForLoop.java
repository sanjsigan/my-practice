
public class ForLoop {

	public static void main(String[] args) {
		int nline = 10;
		int nstar = 1;
		int nspace = nline - 1;

		for (int i = 1; i <= nline; i++) {
			for (int y = 1; y <= nspace; y++)
				System.out.print(" ");
			for (int x = 1; x <= nstar; x++)
				System.out.print("*");
			System.out.println();
			nstar++;
			// nspace--;

		}
		for (int i = 1; i <= 2; i++) {
			nspace = nline + 2;
			for (int j = 1; j <= nspace; j++)
				System.out.print(" ");
			System.out.println("| |");
		}

		for (int i = 1; i <= 1; i++) {
			nspace = nline + 3;
			for (int j = 1; j <= nspace; j++)
				System.out.println("     ");
			System.out.println("| |");
		}

	}

}
