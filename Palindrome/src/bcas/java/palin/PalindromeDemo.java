package bcas.java.palin;

public class PalindromeDemo {
	public static void main(String[] args) {
		Palindrome palin = new Palindrome();

		if (palin.checker("Sanjsigan")) {
			System.out.println("This is palindrome");
		} else {
			System.out.println("Not palindrome");
		}

	}

}
