package bcas.java.martix;

public class DemoMatrix {
	public static void main(String[] args) {
		MyMatrix Matrix = new MyMatrix(5, 5);
		int[][] matrixA = Matrix.arrayRandom();
		int[][] matrixB = Matrix.arrayRandom();

		Matrix.printArray(matrixA);
		System.out.println("\n**********************\n");
		Matrix.printArray(matrixB);
		System.out.println("\n**********************\n");
		Matrix.printArray(Matrix.sum(matrixA, matrixB));
		System.out.println("\n**********************\n");
		Matrix.printArray(Matrix.sub(matrixA, matrixB));

	}

}
