package bcas.java.martix;

import java.util.Random;

public class MyMatrix {
	Random ran = new Random();
	int row, col;

	public MyMatrix(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public int[][] arrayRandom() {

		int[][] arrayMatrix = new int[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				int randValue = ran.nextInt(50);
				if (randValue > 9) {
					arrayMatrix[i][j] = randValue;
				} else {
					j--;
				}

			}
		}
		return arrayMatrix;

	}

	public void printArray(int matrix[][]) {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print("[" + matrix[i][j] + "]");
			}
			System.out.println();
		}

	}

	public int[][] sum(int[][] matrixA, int[][] matrixB) {
		int sum[][] = new int[row][col];

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				sum[i][j] = matrixA[i][j] + matrixB[i][j];
			}
		}
		return sum;

	}

	public int[][] sub(int[][] matrixA, int[][] matrixB) {
		int sub[][] = new int[row][col];

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				sub[i][j] = matrixA[i][j] - matrixB[i][j];
			}
		}
		return sub;

	}
}
