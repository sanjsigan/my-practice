package bcas.java.collection2;

import java.util.ArrayList;
import java.util.List;

public class BcasList {
	private static final String firstletter = "S";

	public static void main(String[] args) {

		List<String> CSD = new ArrayList<String>();
		CSD.add("Sathavi");
		CSD.add("Pranavan");
		CSD.add("Sanjsigan");
		
		System.out.println("__________CSD________");
		displayList(CSD);

		List<String> CIVIL = new ArrayList<String>();
		CIVIL.add("Santhan");
		CIVIL.add("Prabhu");
		CIVIL.add("Mayuran");
		System.out.println("__________CIVIL________");
		displayList(CIVIL);

		List<String> BCAS = new ArrayList<String>();
		BCAS.addAll(CIVIL);
		BCAS.addAll(CSD);
		System.out.println("__________BCAS________");
		displayList(BCAS);
		System.out.println("__________S Strat BCAS________");
		displayList(SnameList(BCAS));
	}

	private static List<String> SnameList(List<String> list) {
		List<String> SnameList = new ArrayList<>();

		for (String name : list) {

			if (Character.toString(name.charAt(0)).toUpperCase().equals(firstletter)) {
				SnameList.add(name);

			}

		}
		return SnameList;

	}

	private static void displayList(List<String> list) {

		for (Object object : list) {
			System.out.println(object);
		}
		System.out.println("________________________");
	}

}
