package bcas.java.collection3;

import java.util.List;
import java.util.ArrayList;

public class DetailsDemo {
	public static void main(String[] args) {

		Department csd = new Department("CSD", 90);
		Department qs = new Department("QS", 80);
		Department civil = new Department("CIVIL", 80);

		List<Department> depaList = new ArrayList<>();
		depaList.add(csd);
		depaList.add(qs);
		depaList.add(civil);

		Organization organization = new Organization("BCAS", "Jaffna", depaList);

		Marks SanjsiDSAmarks = new Marks("DSA", 80);
		Marks SanjsiPRGmarks = new Marks("PRG", 80);
		Marks SanjsiADmarks = new Marks("AD", 80); 

		List<Marks> SanjsiMarks = new ArrayList<>();
		SanjsiMarks.add(SanjsiPRGmarks);
		SanjsiMarks.add(SanjsiADmarks);
		SanjsiMarks.add(SanjsiDSAmarks);

		StudentDetails sanjsi = new StudentDetails(154, "Sanjsi", 19, 076704, csd, SanjsiMarks);
		System.out.println(sanjsi);

		Marks MathavDSAmarks = new Marks("DSA", 81);
		Marks MathavPRGmarks = new Marks("PRG", 60);
		Marks MathavADmarks = new Marks("AD", 89);

		List<Marks> MathavMarks = new ArrayList<>();
		MathavMarks.add(MathavPRGmarks);
		MathavMarks.add(MathavADmarks);
		MathavMarks.add(MathavDSAmarks);

		StudentDetails mathav = new StudentDetails(155, "Mathav", 19, 076474, csd, SanjsiMarks);
		System.out.println(mathav);

		Marks SajanDSAmarks = new Marks("DSA", 85);
		Marks SajanPRGmarks = new Marks("PRG", 84);
		Marks SajanADmarks = new Marks("AD", 90);

		List<Marks> SajanMarks = new ArrayList<>();
		SajanMarks.add(SajanPRGmarks);
		SajanMarks.add(SajanADmarks);
		SajanMarks.add(SajanDSAmarks);

		StudentDetails sajan = new StudentDetails(155, "Mathav", 19, 076474, csd, SanjsiMarks);
		System.out.println(sajan);

		Marks VimalDSAmarks = new Marks("DSA", 85);
		Marks VimaPRGmarks = new Marks("PRG", 84);
		Marks VimaADmarks = new Marks("AD", 90);

		List<Marks> VimaMarks = new ArrayList<>();
		VimaMarks.add(VimaPRGmarks);
		VimaMarks.add(VimaADmarks);
		VimaMarks.add(VimalDSAmarks);

		StudentDetails vimal = new StudentDetails(155, "Mathav", 19, 076474, csd, SanjsiMarks);
		System.out.println(vimal);

	}

}
