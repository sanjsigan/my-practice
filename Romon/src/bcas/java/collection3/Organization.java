package bcas.java.collection3;

import java.util.List;

public class Organization {
	private String name;
	private String address;
	private List<Department> department;

	public Organization(String name, String address, List<Department> department) {

		this.name = name;
		this.address = address;
		this.department = department;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Department> getDepartment() {
		return department;
	}

	public void setDepartment(List<Department> department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Organization [name=" + name + ", address=" + address + ", department=" + department + "]";
	}

}
