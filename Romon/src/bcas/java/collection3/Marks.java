package bcas.java.collection3;

public class Marks {
	private String subect;
	private double marks;

	public Marks(String subect, double marks) {

		this.subect = subect;
		this.marks = marks;

	}

	public String getSubect() {
		return subect;
	}

	public void setSubect(String subect) {
		this.subect = subect;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Marks [subect=" + subect + ", marks=" + marks + "]";
	}

}
