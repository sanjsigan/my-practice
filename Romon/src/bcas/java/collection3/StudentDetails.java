package bcas.java.collection3;

import java.util.List;

public class StudentDetails {
	private int RegNo;
	private String name;
	private int age;
	private int contact;
	private Department department;
	private List<Marks> marks;
	private List<Departtyoe> deptyoe;

	public StudentDetails(int regNo, String name, int age, int contact, Department department, List<Marks> marks) {
		RegNo = regNo;
		this.name = name;
		this.age = age;
		this.contact = contact;
		this.department = department;
		this.marks = marks;
	}

	public int getRegNo() {
		return RegNo;
	}

	public void setRegNo(int regNo) {
		RegNo = regNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getContact() {
		return contact;
	}

	public void setContact(int contact) {
		this.contact = contact;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Marks> getMarks() {
		return marks;
	}

	public void setMarks(List<Marks> marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "StudentDetails [RegNo=" + RegNo + ", name=" + name + ", age=" + age + ", contact=" + contact
				+ ", department=" + department + ", marks=" + marks + "]";
	}

}
