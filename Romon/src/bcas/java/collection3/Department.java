package bcas.java.collection3;

import java.util.List;

public class Department {
	private String name;
	private int noOfSudents;
	

	public Department(String name, int noOfSudents) {

		this.name = name;
		this.noOfSudents = noOfSudents;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoOfSudents() {
		return noOfSudents;
	}

	public void setNoOfSudents(int noOfSudents) {
		this.noOfSudents = noOfSudents;
	}

	@Override
	public String toString() {
		return "Department [name=" + name + ", noOfSudents=" + noOfSudents + "]";
	}

}
