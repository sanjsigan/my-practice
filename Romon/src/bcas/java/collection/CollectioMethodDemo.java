package bcas.java.collection;

import java.util.List;
import java.util.ArrayList;

public class CollectioMethodDemo {
	public static void main(String[] args) {
		List<String> nameList = new ArrayList<String>();
		nameList.add("Mathavi");
		nameList.add("Pranavan");
		nameList.add("Sanjsigan");
		displayList(nameList);

		nameList.add(1, "Sanjsi");
		displayList(nameList);

		nameList.remove(2);
		displayList(nameList);

		List<String> nameListA = new ArrayList<String>();
		nameListA.add("Mayuran");
		nameListA.add("Pethu");
		nameListA.add("Prathap");
		displayList(nameListA);

		List<String> nameListB = new ArrayList<String>();
		nameListB.add("Mayuran");
		nameListB.add("Prana");
		nameListB.add("Prathap");
		displayList(nameListB);

		List<String> nameListC = new ArrayList<String>();
		nameListC.add("Mayuran");
		nameListC.add("Pethu");
		nameListC.add("Prathap");
		displayList(nameListB);
		/*
		 * nameListA.addAll(nameListB); System.out.println("__add all_A,B");
		 * displayList(nameListA);
		 */

		System.out.println("size of nameListA = " + nameListA.size());

		System.out.println("contains of nameListA = " + nameListA.contains("Mayuran"));
		System.out.println("contains of nameListB = " + nameListB.contains("Mayuran"));

		System.out.println("contains all nameLiseA,nameListB = " + nameListA.containsAll(nameListC));

		System.out.println("nameListA equals nameListB = " + nameListA.equals(nameListC));
		
		System.out.println(nameListA.isEmpty());
		
		System.err.println();

	}

	private static void displayList(List<String> list) {
		for (Object object : list) {
			System.out.println(object);

		}
		System.out.println("________________________");
	}

}
