package bcas.java.romon;

import java.util.Scanner;

public class RomonDemo {
	public static void main(String[] args) {

		int number;
		boolean fact = true;

		Scanner scan = new Scanner(System.in);
		IntegerToRomon romon = new IntegerToRomon();
		do {
			System.out.println("Enter Your Number: ");
			number = scan.nextInt();
			System.out.println(romon.intToRoman(number));
		} while (fact);

	}

}
