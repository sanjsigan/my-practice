package bcas.java.romon;

public class IntegerToRomon {

	public String intToRoman(int num) {
		String romanNum = "";
		int[] intArr = { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
		String[] romanArr = { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };
		for (int i = intArr.length-1; i >= 0; i--) {
			while (num - intArr[i] > 0) {
				num = num - intArr[i];
				romanNum = romanNum + romanArr[i];
			}
		}
		return romanNum;
	}
}
