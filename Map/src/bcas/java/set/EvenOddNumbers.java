package bcas.java.set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EvenOddNumbers {
	public static void main(String[] args) {
		List<Integer> number = Arrays.asList(1, 2, 3, 4, 8, 10, 14, 45, 7 b5	8, 11, 16, 71, 78);
		List<Integer> ODD = new ArrayList<>();
		List<Integer> EVEN = new ArrayList<>();

		ODD.addAll(number);
		System.out.println("ODD NUMBER");
		for (Integer odd : ODD) {
			if (odd % 2 == 0) {
				System.out.println(odd);
			}
		}
		EVEN.addAll(number);
		System.out.println("EVEN NUMBER");
		for (Integer even : EVEN) {
			if (even % 2 != 0) {
				System.out.println(even);
			}
		}

	}

}
