package bcas.java.set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class CSD {
	public static void main(String[] args) {
		List<String> CSD12 = Arrays.asList("kamal", "pethu", "prana", "sanjsi", "logi", "vinthusha", "sajan", "tharan");
		List<String> CSD11 = Arrays.asList("kamal", "sajan", "tharan");
		List<String> DSA = new ArrayList<>();

		DSA.addAll(CSD11);
		DSA.addAll(CSD12);

		System.out.println("_CSD12_");
		for (String csd12 : DSA) {

			System.out.println(csd12);

		}
		System.out.println();
		System.out.println("_CSD11_");
		for (String csd11 : DSA) {

			System.out.println(csd11);

		}
		System.out.println();
		System.out.println("REMOVE___");
		SortedSet<String> remove = new TreeSet<>(DSA);
		for (String string : remove) {
			System.out.println(string);

		}

	}

}
