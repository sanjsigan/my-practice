package bcas.java.set;

import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetDemo {
	public static void main(String[] args) {
		List<String> numList = Arrays.asList("Arun", "Sam", "Samantha", "Kathir", "Aathi");
		Set<String> numSet = new HashSet<>(numList);
		SortedSet<String> numSets = new TreeSet<>(numList);

		for (String string : numSets) {

			System.out.println(string);

		}

		System.out.println();
		for (String string : numSet) {

			System.out.println(string);

		}
	}

}
