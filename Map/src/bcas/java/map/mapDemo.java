package bcas.java.map;

import java.util.HashMap;
import java.util.Map;

public class mapDemo {
	public static void main(String[] args) {
		Map<String, Student> stuMap = new HashMap<>();

		Student Mathu = new Student("Mathu", 92);
		Student kavin = new Student("kavin", 80);
		Student Jana = new Student("Jana", 70);

		stuMap.put("Mathu", Mathu);
		stuMap.put("kavin", kavin);
		stuMap.put("Jana", Jana);

		// stuMap.remove("Mathu");

		System.out.println(stuMap.get("Mathu"));
		System.out.println(stuMap.get("kavin"));
		System.out.println(stuMap.get("Jana"));

		System.out.println();

		System.out.println(stuMap.keySet());
		System.out.println(stuMap.values());
		System.out.println(stuMap.entrySet());

	}

}qw	
