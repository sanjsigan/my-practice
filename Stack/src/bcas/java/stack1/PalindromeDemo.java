package bcas.java.stack1;

import java.util.Scanner;

public class PalindromeDemo {
	public static void main(String[] args) {
		String popString = "";
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the value");
		String read = scan.nextLine();
		String name = read.toUpperCase();
 
		System.out.println("your value is : " + name);
		ReverseStack reverseStack = new ReverseStack(10);

		for (int i = 0; i < name.length(); i++) {
			reverseStack.push(name.charAt(i));

		}

		while (!reverseStack.isempty()) {
			popString = popString + Character.toString(reverseStack.pop());

		}

		System.out.println();

		if (popString.equals(name)) {
			System.out.println(name + " is a palindrome");
		} else {
			System.out.println(name + " is not palindrome");
		}
	}
}
