package bcas.java.stack1;

public class ReverseStack {
	private int maxsize;
	String reverse = "";
	char[] stackArray = reverse.toCharArray();
	private int top;

	public ReverseStack(int m) {
		maxsize = m;
		stackArray = new char[m];
		top = -1;

	}

	public void push(char j) {
		stackArray[++top] = j;

	}

	public char pop() {
		return stackArray[top--];

	}

	public char peek() {
		return stackArray[top];

	}

	public boolean isempty() {
		return (top == -1);

	}

	public boolean isfull() {
		return (top == maxsize - 1);

	}

	public void isfull1() {
		// { if (top== maxsize-1)?0;}

	}

	/*
	 * public void printarray() { for (char l : stackArray) {
	 * System.out.println(stackArray);
	 * 
	 * }
	 * 
	 * }
	 */
}
