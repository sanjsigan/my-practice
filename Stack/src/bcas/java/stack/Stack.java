
package bcas.java.stack;

public class Stack {
	public long[] StackArray;
	int top;
	private int mySize;

	public Stack(int mySize) {
		this.mySize = mySize;
		StackArray = new long[mySize];
		top = -1;

	}

	public void push(long pushedElement) {
		StackArray[++top] = pushedElement;

	}

	public long pop() {

		return StackArray[top--];

	}

	public long peek() {
		return StackArray[top];

	}

	public boolean isEmpty() {
		return (top == -1);

	}

	public boolean isFull() {
		return (top == mySize - 1);

	}

}
