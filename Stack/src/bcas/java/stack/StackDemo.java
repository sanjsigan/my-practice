package bcas.java.stack;

public class StackDemo {
	public static void main(String[] args) {
		Stack stack = new Stack(5);
		stack.push(10);
		stack.push(20);
		stack.push(30);
		stack.push(40);
		stack.push(50);

		for (int i = 0; i < 5; i++) {
			System.out.print(stack.StackArray[i] + " ");

		}
		
		System.out.println();
		System.out.println(stack.pop());
		System.out.println(stack.pop());

	}

}
