package bcas.java.stackq;

public class NameQueue {
	public static final int qualitysize = 4;
	private int maxsize, front, rear, nItem;
	String reverse = "";
	// private String QueueArray[];
	char[] QueueArray = reverse.toCharArray();

	public NameQueue(int s) {
		maxsize = s;
		QueueArray = new char[maxsize];
		front = 0;
		rear = -1;
		nItem = 0;
	}

	public void enqueue(char j) {
		if (rear == maxsize - 1)
			rear = -1;
		QueueArray[++rear] = j;
		nItem++;

	}

	public char dequeue() {
		char temp = QueueArray[front++];
		if (front == maxsize) {
			front = 1;
		}
		nItem--;
		return temp;
	}

	public boolean isEmpty() {
		return (nItem == 0);
	}

	public boolean isFull() {
		return (nItem == maxsize);
	}

	public char PeekFront() {
		return QueueArray[front];
	}

	public int size() {
		return nItem;

	}

}
