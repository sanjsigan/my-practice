package bcas.java.stackq;

import java.util.Scanner;

public class PlaindromeQueue {
	public static void main(String[] args) {
		// ReverseStack stack = new ReverseStack(10);
		NameQueue queue = new NameQueue(10);
		String popString = "";
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Value");
		String read = scan.nextLine();
		String name = read.toUpperCase();

		System.out.println("Your Value is : " + name);

		for (int i = name.length() - 1; i >= 0; i--) {
			queue.enqueue(name.charAt(i));

		}

		while (!queue.isEmpty()) {
			popString = popString + Character.toString(queue.dequeue());

		}
		if (name.equals(popString))
			System.out.println("The input String is a palindrome.");
		else
			System.out.println("The input String is not a palindrome.");

	}

}
