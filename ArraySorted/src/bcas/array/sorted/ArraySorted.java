package bcas.array.sorted;

import java.util.Scanner;

public class ArraySorted {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your Number: ");
		int n = scan.nextInt();

		int[] arr = new int[n];
		int i, j, index = 0;

		System.out.println("Enter " + n + " Elements Array:");
		for (i = 0; i < n; i++) {
			arr[i] = scan.nextInt();
		}
		System.out.print("\nArray elements are:");
		for (i = 0; i < n; i++) {
			System.out.print(" " + arr[i]);
		}
		for (i = 0; i < n; i++) {
			for (j = i + 1; j < n; j++) {
				if (arr[i] > arr[j]) {
					index = arr[i];
					arr[i] = arr[j];
					arr[j] = index;
				}
			}
		}
		System.out.print("\nAssesding order is here:");
		for (i = 0; i < n; i++) {
			System.out.print(" " + arr[i]);
		}
		for (i = 0; i < n; i++) {
			for (j = i + 1; j < n; j++) {
				if (arr[i] < arr[j]) {
					index = arr[i];
					arr[i] = arr[j];
					arr[j] = index;
				}
			}
		}
		System.out.print("\nDesending order is here:");
		for (i = 0; i < n; i++) {
			System.out.print(" " + arr[i]);
		}
	}
}
