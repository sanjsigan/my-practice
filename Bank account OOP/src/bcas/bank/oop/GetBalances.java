package bcas.bank.oop;

import java.util.Scanner;

public class GetBalances implements BankDetails {
	Scanner scan = new Scanner(System.in);
	
	public GetBalances(Scanner scan) {
		super();
		this.scan = scan;
	}

	@Override
	public double Deposit(double Deposit, double accBal) {
		System.out.println("Enter  Your Deposit: ");
		Deposit = scan.nextDouble();
		System.out.println("Enter your accBal: ");
		accBal=scan.nextDouble();
		return Deposit + accBal;
	}

	@Override
	public double Widthdraw(double accBal, double Withdraw) {

		return accBal - Withdraw;
	}

	@Override
	public double ClosingBal(double accbal, double ClosingBal) {

		return accbal + ClosingBal;
	}

}
