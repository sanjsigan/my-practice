
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;

public class AppletDemo extends Applet implements ActionListener {

	TextField t1, t2;
	Label l1, l2, l3;
	Button b1;

	public void init() {
		l1 = new Label("Enter Your Name:");
		t1 = new TextField(20);
		add(l1);
		add(t1);

		l2 = new Label("Enter Your Password: ");
		t2 = new TextField(20);
		add(l2);
		add(t2);

		b1 = new Button("Login");
		add(b1);
		b1.addActionListener(this);
		l3 = new Label("Please login:");
		add(l3);

	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == b1) {
			l3.setText("Login SuucessFull..");

		}

	}

}
