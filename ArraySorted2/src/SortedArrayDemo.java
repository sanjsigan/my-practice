
public class SortedArrayDemo {
	public static void main(String[] args) {

		int unSortedArray[] = { 20, 10, 20, 70, 90, 30 };

		SortedArrayApi sortAPI = new SortedArrayApi();

		// System.out.println("Minimum value is : " +
		// sortAPI.findMinimumValue(unSortedArray));\
		
		// System.out.println("Minimum index is : " +
		// sortAPI.findMinimumIndex(unSortedArray));

		
		int size = SortedAray.readUserInput("Enter the size of array");
		int range = SortedAray.readUserInput("Enter the random range");

		int unSortedRandomArray[] = SortedAray.generateRandomArray(size, range);

		System.out.print("Array befor sort : ");
		SortedAray.printArray(unSortedRandomArray);

		System.out.print("\nArray after sort : ");
		SortedAray.printArray(sortAPI.sortArray(unSortedRandomArray));

	}

}
