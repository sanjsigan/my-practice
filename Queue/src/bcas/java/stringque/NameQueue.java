package bcas.java.stringque;

public class NameQueue {
	public int capacity = 3;
	// String name = "";
	String arr[] = new String[capacity];
	int size = 0;
	int top = -1;
	int rear = 0;

	public void enqueue(String pushElement) {
		if (top < capacity - 1) {
			top++;
			arr[top] = pushElement;
		}
	}

	public void dequeue() {
		if (top >= rear) {

			display();
			rear++;
		}

	}

	public void display() {
		if (top >= rear) {
			for (int i = rear; i <= top; i++) {
				System.out.println(arr[i]);

			}
		}

	}
	public String mixName(){
		return arr[size++];
		
	}

	public boolean isEmpty() {
		return (top == -1);

	}

	public boolean isFull() {
		return (top == capacity );

	}

}
