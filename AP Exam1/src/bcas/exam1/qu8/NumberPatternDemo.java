package bcas.exam1.qu8;

public class NumberPatternDemo {
	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= i; j++) {
				System.out.print(j + " * ");
				System.out.print("/");
			}

			System.out.println();
		}
	}
}
