package bcas.exam1.qu1;

public class A {
	static {
		System.out.println("Static");
	}

	private A() {
		System.out.println("A");
	}

	public static void main(String[] args) {
		A a = new A();
	}

}
