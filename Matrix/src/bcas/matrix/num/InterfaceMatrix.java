package bcas.matrix.num;

public interface InterfaceMatrix {
	public int[][] arrayRandom();

	public void printArray(int matrix[][]);

	public int[][] sum(int[][] matrixA, int[][] matrixB);

}
