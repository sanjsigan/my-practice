package bcas.matrix.num;

import java.util.Random;

public class Matrix implements InterfaceMatrix {

	Random ran = new Random();

	public int row;
	public int col;

	@Override
	public int[][] arrayRandom() {
		int[][] arrayMatrix = new int[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				int randValue = ran.nextInt(5);
				if (randValue > 2) {
					arrayMatrix[i][j] = randValue;
				} else {
					j--;
				}

			}
		}
		return arrayMatrix;
	}

	@Override
	public void printArray(int[][] matrix) {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {

				System.out.print("[" + matrix[i][j] + "]" + " ");
			}
			System.out.println();
		}

	}

	@Override
	public int[][] sum(int[][] matrixA, int[][] matrixB) {
		int sum[][] = new int[row][col];

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				for (int k = 0; k < row; k++)
					sum[i][j] += matrixA[i][k] * matrixB[k][j];
			}
		}
		return sum;
	}

}
