package bcas.ap.arrword;

import java.util.Random;
import java.util.Scanner;

public class ArrayWords {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int length;
		System.out.println("Enter your Number of friends: ");
		length = scan.nextInt();

		String[] name = new String[length];

		for (int counter = 0; counter < length; counter++) {
			System.out.println("Enter your Friends name: " + (counter + 1));
			name[counter] = scan.next();

		}
		scan.close();
		Random random = new Random();
		int randomNumber = random.nextInt(name.length);
		System.out.println("Your Friends are: ");
		System.out.println(name[randomNumber]);

	}
}
