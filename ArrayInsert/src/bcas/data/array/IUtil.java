package bcas.data.array;

public interface IUtil {
	

	public int[] generateRandomRange(int Arrlength, int bound);

	public int[] generateRandomRange(int Arrlength, int startBound, int endBound);

}
