package bcas.data.array;

import java.util.Random;
import java.util.Scanner;

public class DetUtil implements IUtil {
	Scanner scan = new Scanner(System.in);
	Random random = new Random();

	

	@Override
	public int[] generateRandomRange(int Arrlength, int bound) {
		int randomarry[] = new int[Arrlength];
		for (int i = 0; i < Arrlength; i++) {
			randomarry[i] = random.nextInt(bound);

		}

		return randomarry;
	}

	@Override
	public int[] generateRandomRange(int Arrlength, int startBound, int endBound) {
		int randomarray[] = new int[Arrlength];
		for (int i = 0; i < Arrlength; i++) {
			randomarray[i] = random.nextInt(endBound);
		}

		return randomarray;
	}

}
