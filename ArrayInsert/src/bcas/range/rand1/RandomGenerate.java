package bcas.range.rand1;

import java.util.Random;
import java.util.Scanner;

public class RandomGenerate implements IUtil {
	Scanner scan = new Scanner(System.in);
	Random rand = new Random();

	@Override
	public int generateRandomrange(int max) {
		System.out.println("Enter Your Max Number: ");
		max = scan.nextInt();

		for (int c = 1; c <= max; c++) {
			System.out.print(rand.nextInt(max) + " ");
		}

		return max;
	}

	@Override
	public String Letters(String letter, int length) {
		System.out.println("Enter your Length: ");
		length = scan.nextInt();
		letter = "ABCDEFGHIJKLMNOPQRSTVUWZYZ";
		for (int i = 1; i <= length; i++) {
			System.out.print(letter.charAt(rand.nextInt(letter.length())) + " ");
		}
		return null;
	}

}
