
public class FibonacciExample2 {
	static int n1 = 0;
	static int n2 = 1;
	static int n3 = 0;

	public static void fib(int num) {
		if (num > 0) {

			n3 = n1 + n2;

			n1 = n2;
			n2 = n3;
			System.out.print("  " + n3);
			fib(num - 1);
		}

	}

	public static void main(String[] args) {
		int num = 10;
		System.out.print(n1 + "  " + n2);
		fib(num - 2);

	}

}
