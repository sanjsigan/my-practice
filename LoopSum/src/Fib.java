
public class Fib {

	public int Total(int num) {
		if (num <= 1) {
			return num;
		}
		return Total(num - 1) + Total(num - 2);
		
	}

}
